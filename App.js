import "react-native-get-random-values";
import "@ethersproject/shims";
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, SafeAreaView } from 'react-native';
import Colors from "./constant/color";
import { NavigationContainer } from "@react-navigation/native";
import MainNavigator from "./Navigators/MainNavigator";
import { Provider } from "react-redux";
import { store } from "./Store/store";


export default function App (){
  return (
     <SafeAreaView style={styles.container}>
     <Provider store={store}>
    <NavigationContainer>
      <MainNavigator/>
    </NavigationContainer>
    </Provider>
    <StatusBar style="dark" backgroundColor={Colors.secondary} />
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D2EFFF',
    },
  text: {
    fontSize: 25,
    fontWeight: '500',
  },
secondary:{
    primary: "#FFCDB2",
    primary2: "#FFDFC9",
    secondary: "#FFB4A2",
    secondary2: "#FFCEBD",
    background: "#D2EFFF",
    backgroundWhite: "white",
    backgroundGrey: "#ffffff",
    text: "#333333",
  }
});